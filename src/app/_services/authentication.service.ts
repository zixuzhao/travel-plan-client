import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    }),
};

@Injectable({providedIn: 'root'})
export class AuthenticationService {
    public currentUser: Observable<User>;
    private currentUserSubject: BehaviorSubject<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public register(user: User) {
        return this.http.post(`${environment.apiUrl}/auth/register`, user, httpOptions);
    }

    public updateUser(user: User) {
        this.currentUserSubject.next(user);
    }

    public login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/auth/login`, {username, password}, httpOptions)
            .pipe(
                map(credential => {
                    // login successful if there's a jwt token in the response
                    if (credential && credential.success && credential.user && credential.token) {
                        // store user details and jwt token in local storage to
                        // keep user logged in between page refreshes
                        const user = new User(credential.user, credential.token);
                        localStorage.setItem('currentUser', JSON.stringify(user));
                        this.currentUserSubject.next(user);
                    }
                    return credential;
                }));
    }

    public logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
