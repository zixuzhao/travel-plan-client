import { LocalDataSource } from 'ng2-smart-table';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "@environments/environment";
import { AlertService } from "@app/_services/alert.service";
import * as moment from "moment";

@Injectable({providedIn: 'root'})
export class TripSourceService extends LocalDataSource {

    lastRequestCount: number = 0;
    params: string = '';
    private base = '';

    constructor(private endPoint: string,
                private http: HttpClient,
                private alertService: AlertService,
                private nonScope: boolean,
                private message: boolean) {
        super();
    }

    count(): number {
        return this.lastRequestCount;
    }

    getElements(): Promise<object> {
        let params = '';
        this.base = `${environment.apiUrl}${this.endPoint}?`;

        if (this.filterConf.filters) {
            this.filterConf.filters.forEach((fieldConf: any) => {
                if (fieldConf['search']) {
                    params += `${fieldConf['field']}=${fieldConf['search']}&`;
                }
            });
        }
        if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
            params += `size=${this.pagingConf['perPage']}&page=${this.pagingConf['page']}`;
        }
        this.params = params;
        return this.http.get(this.base + this.params)
            .toPromise()
            .then((data: { data: object[], count: number }) => {
                this.lastRequestCount = data.count;
                if (this.message) {
                    this.alertService.success("Fetch Trips Successfully");
                }
                const response = data.data;
                response.forEach(e => {
                    const currentStartOfDay = moment().local().startOf('day');
                    const daysLeft = moment(e['startTime']).local().startOf('day').diff(currentStartOfDay, 'days');
                    e['daysLeft'] = TripSourceService.calculateDaysLeft(daysLeft);
                    e['startTime'] = moment(e['startTime']).format('M/DD/YYYY');
                    e['endTime'] = moment(e['endTime']).format('M/DD/YYYY');

                    if (this.nonScope) {
                        e['email'] = e['user']['email'];
                    }
                });
                return response;
            })
            .catch(error => {
                this.alertService.error(error);
                return [];
            });
    }

    find(e): Promise<object> {
        return new Promise((resolve, reject) => {
            resolve(e);
        });
    }

    static calculateDaysLeft(daysLeft) {
        if (daysLeft === 0) {
            return 'Today';
        } else if (daysLeft === 1) {
            return 'Tomorrow';
        } else if (daysLeft === -1) {
            return 'Yesterday';
        } else if (daysLeft > 1) {
            return `In ${daysLeft} Days`;
        } else {
            return `${0 - daysLeft} Days Ago`;
        }

    }
}
