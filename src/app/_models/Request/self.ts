export class Self {
    public username: string;
    public password: string;
    public firstName: string;
    public lastName: string;
    public email: string;

    constructor(self) {
        this.username = self.username;
        this.firstName = self.firstName;
        this.lastName = self.lastName;
        this.email = self.email;
        this.password = self.password;
    }
}
