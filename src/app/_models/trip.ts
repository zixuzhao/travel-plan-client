import { User } from "@app/_models/user";

export class Trip {
    public country: string;
    public state: string;
    public city: string;
    public address: string;
    public zipCode: string;
    public startTime: string;
    public endTime: string;
    public comment: string;
    public user: User;

    static toList(daysLeft, trip: Trip) {
        return [daysLeft, trip.country, trip.state, trip.city, trip.address,
            trip.zipCode, trip.startTime, trip.endTime, trip.comment];
    }

    static toListWithUser(daysLeft, trip: Trip) {
        return [daysLeft, trip.country, trip.state, trip.city, trip.address,
            trip.zipCode, trip.user.email, trip.startTime, trip.endTime, trip.comment];
    }
    constructor(trip) {
        this.country = trip.country;
        this.state = trip.state;
        this.city = trip.city;
        this.address = trip.address;
        this.zipCode = trip.zipCode;
        this.startTime = trip.startTime;
        this.endTime = trip.endTime;
        this.comment = trip.comment;
        this.user = trip.user;
    }
}
