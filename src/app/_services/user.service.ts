import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Self } from "@app/_models/Request/self";

@Injectable({providedIn: 'root'})
export class UserService {
    constructor(private http: HttpClient) {
    }

    public getAuth(id: string, password: string) {
        return this.http.post(`${environment.apiUrl}/auth/validate`, {id, password});
    }

    public selfUpdate(user: Self) {
        return this.http.put(`${environment.apiUrl}/users/self/update`, user);
    }

    public selfDelete() {
        return this.http.delete(`${environment.apiUrl}/users/self/delete`);
    }

    public selfTruncate() {
        return this.http.delete(`${environment.apiUrl}/users/self/truncate`);
    }

    public userCreate(user: object) {
        return this.http.post(`${environment.apiUrl}/users/create`, user);
    }

    public userUpdate(id: string, user: object) {
        return this.http.put(`${environment.apiUrl}/users/update/${id}`, user);
    }

    public userTruncate(id: string) {
        return this.http.delete(`${environment.apiUrl}/users/truncate/${id}`);
    }

    public userGetOne(id) {
        return this.http.get(`${environment.apiUrl}/users/search/${id}`);
    }

    public adminUpdate(id: string, user: object) {
        return this.http.put(`${environment.apiUrl}/admin/user/update/${id}`, user);
    }
}
