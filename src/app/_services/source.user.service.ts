import { LocalDataSource } from 'ng2-smart-table';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "@environments/environment";
import { AlertService } from "@app/_services/alert.service";

@Injectable({providedIn: 'root'})
export class UserSourceService extends LocalDataSource {

    lastRequestCount: number = 0;
    params: string = '';
    private base = '';

    constructor(private endPoint: string,
                private http: HttpClient,
                private alertService: AlertService,
                private admin: boolean) {
        super();
    }

    count(): number {
        return this.lastRequestCount;
    }

    getElements(): Promise<object> {
        let params = '';
        this.base = `${environment.apiUrl}${this.endPoint}?`;

        if (this.filterConf.filters) {
            this.filterConf.filters.forEach((fieldConf: any) => {
                if (fieldConf['search']) {
                    params += `${fieldConf['field']}=${fieldConf['search']}&`;
                }
            });
        }
        if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
            params += `size=${this.pagingConf['perPage']}&page=${this.pagingConf['page']}`;
        }
        this.params = params;
        return this.http.get(this.base + this.params)
            .toPromise()
            .then((data: { data: object[], count: number }) => {
                this.lastRequestCount = data.count;
                this.alertService.success("Fetch Users Successfully");
                const response = data.data;
                response.forEach((e: {id: string}) => {
                    e['password'] = '';
                    if (this.admin) {
                        e['userTrips'] = `<a href="/user/trips/${e.id}">View Trips</a>`
                    }
                });
                return response;
            })
            .catch(error => {
                this.alertService.error(error);
                return [];
            });
    }

    find(e): Promise<object> {
        return new Promise((resolve, reject) => {
            resolve(e);
        });
    }
}
