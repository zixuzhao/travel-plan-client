import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { User } from '@app/_models';
import { AlertService, AuthenticationService, UserService } from '@app/_services';
import { HttpClient } from "@angular/common/http";
import { LocalDataSource } from "ng2-smart-table";
import { UserSourceService } from "@app/_services/source.user.service";
import { Role } from "@app/_models/role";
import { first } from "rxjs/operators";
import { Router } from "@angular/router";

@Component({
    templateUrl: 'manager.component.html',
    styleUrls: ['manager.component.css']
})
export class ManagerComponent implements OnInit, OnDestroy {
    public currentUser: User;
    public currentUserSubscription: Subscription;

    public source: LocalDataSource;

    private fetchingData(admin = false): LocalDataSource {
        return new UserSourceService(`/users/all`, this.http, this.alertService, admin);
    }

    constructor(
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private userService: UserService,
        private http: HttpClient,
        private router: Router,
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser
            .subscribe(user => {
                this.currentUser = user;
                if (this.currentUser && this.currentUser.role === Role.ADMIN) {
                    const prev = this.settings.columns;
                    prev['role'] = {
                        title: 'Role',
                        type: 'string',
                        width: '200px',
                        editable: true,
                        editor: {
                            type: 'list',
                            config: {
                                selectText: 'Select',
                                list: [
                                    {value: 'ADMIN', title: 'ADMIN'},
                                    {value: 'MANAGER', title: 'MANAGER'},
                                    {value: 'REGULAR', title: 'REGULAR'},
                                ],
                            },
                        }
                    };
                    prev['userTrips'] = {
                        title: 'Link',
                        type: 'html',
                        width: '150px',
                        filter: false,
                        editable: false,
                        addable: false,
                    };
                    this.settings['columns'] = prev;

                    this.source = this.fetchingData(true);
                } else {
                    this.source = this.fetchingData();
                }
            });
    }

    public ngOnInit() {
    }

    viewAllTrips() {
        this.router.navigate(['/user/trips/all']);
    }
    onCreateConfirm(event): void {
        if (window.confirm('Are you sure you want to create?')) {
            const user = event.newData;
            this.userService.userCreate(user).pipe(first())
                .subscribe(
                    (data: { id: string }) => {
                        if (this.currentUser.role === Role.ADMIN) {
                            this.updateUserRole(data.id, user.role);
                        } else {
                            this.source.refresh();
                        }
                        this.alertService.success("Create User Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    updateUserRole(id, role) {
        if (this.currentUser.role !== Role.ADMIN) {
            this.alertService.error("Only Admin can edit user's role");
            return;
        }
        this.userService.adminUpdate(id, {role}).pipe(first())
            .subscribe(
                data => {
                    this.source.refresh();
                    this.alertService.success("Update User's Role Successfully");
                },
                error => {
                    this.alertService.error(error);
                });
    }

    userUpdate(uid: string, user: User) {
        this.userService.userUpdate(uid, user).pipe(first())
            .subscribe(
                data => {
                    this.source.refresh();
                    this.alertService.success("Update User Successfully");
                },
                error => {
                    this.source.refresh();
                    this.alertService.error(error);
                });
    }

    adminUpdate(uid: string, user: User) {
        this.userService.adminUpdate(uid, user).pipe(first())
            .subscribe(
                data => {
                    this.source.refresh();
                    this.alertService.success("Update User Successfully");
                },
                error => {
                    this.source.refresh();
                    this.alertService.error(error);
                });
    }

    onUpdateConfirm(event) {
        if (window.confirm('Are you sure you want to save?')) {
            const user = event.newData;
            if (this.currentUser.role !== Role.ADMIN) {
                this.userUpdate(user.id, user);
            } else {
                this.adminUpdate(user.id, user);
            }
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    onDeleteConfirm(event) {
        if (window.confirm('Are you sure you want to delete?')) {
            this.userService.userTruncate(event.data.id).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Delete User Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    resetFilters() {
        this.source.reset();
    }

    public ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    public settings = {
        mode: 'inline',
        add: {
            confirmCreate: true,
        },
        edit: {
            confirmSave: true
        },
        delete: {
            confirmDelete: true
        },
        columns: {
            firstName: {
                title: 'First Name',
                type: 'string',
                width: '230px',
            },
            lastName: {
                title: 'Last Name',
                type: 'string',
                width: '230px',
            },
            email: {
                title: 'Email',
                type: 'string',
                width: '300px',
            },
            username: {
                title: 'Username',
                type: 'string',
                width: '230px',
            },
            role: {
                title: 'Role',
                type: 'string',
                width: '230px',
                editable: false,
                editor: {},
            },
            password: {
                title: 'Password',
                type: 'html',
                width: '170px',
                filter: false,
                editable: true,
                valuePrepareFunction: () => {
                    return '<span class="password-cell">******</span>';
                },
            }
        },
        pager: {
            display: true,
            perPage: 8
        }
    };
}
