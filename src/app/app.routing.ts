import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { ManagerComponent } from "./manager";
import { ProfileComponent } from "@app/profile";
import { TripsComponent } from "@app/trips";
import { PermissionGuard } from "@app/_guards/permission.guard";

const appRoutes: Routes = [
    {path: '', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'administration', component: ManagerComponent, canActivate: [AuthGuard]},
    {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    {path: 'user/trips/:id', component: TripsComponent, canActivate: [AuthGuard, PermissionGuard]},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},



    // otherwise redirect to home
    {path: '**', redirectTo: ''},
];

export const routing = RouterModule.forRoot(appRoutes);
