import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorInterceptor, JwtInterceptor } from './_helpers';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_components';
import { HomeComponent } from './home';
import { TripsComponent } from './trips';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { ManagerComponent } from "./manager";
import { ProfileComponent } from "./profile";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DatepickerEditorComponent, DatepickerRenderComponent } from './datepicker'
import { DatepickerFilterComponent } from './datepicker'

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        NgbModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        FormsModule,
        Ng2SmartTableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        BrowserAnimationsModule,
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        TripsComponent,
        LoginComponent,
        RegisterComponent,
        ManagerComponent,
        ProfileComponent,
        DatepickerEditorComponent,
        DatepickerRenderComponent,
        DatepickerFilterComponent
    ],
    entryComponents: [
        DatepickerEditorComponent,
        DatepickerRenderComponent,
        DatepickerFilterComponent
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    ],
    bootstrap: [AppComponent],
})

export class AppModule {
}
