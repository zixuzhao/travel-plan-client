import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { LocalDataSource } from 'ng2-smart-table';
import { User } from '@app/_models';
import { AlertService, AuthenticationService, UserService } from '@app/_services';
import { DatepickerEditorComponent, DatepickerFilterComponent, DatepickerRenderComponent } from "@app/datepicker";
import { TripService } from "@app/_services/trip.service";
import { TripSourceService } from "@app/_services/source.trip.service";
import { HttpClient } from "@angular/common/http";
import { first, pluck, takeUntil } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";
import { Trip } from "@app/_models/trip";
import JsPDF from 'jspdf';
import 'jspdf-autotable';
import * as moment from "moment";

@Component({
    templateUrl: 'trips.component.html',
    styleUrls: ['trips.component.css']
})
export class TripsComponent implements OnInit, OnDestroy {
    public currentUser: User;
    public scopeUser: User;
    public currentUserSubscription: Subscription;
    private scopeUserSubject = new Subject<any>();

    public startTimeModel: Date;
    public endTimeModel: Date;

    public allOfTrips: boolean = false;
    public timeSlot: boolean = true;

    public source: LocalDataSource;
    public settings = {};

    public userId: string;

    public doc: JsPDF;

    constructor(
        private authenticationService: AuthenticationService,
        private tripService: TripService,
        private userService: UserService,
        private alertService: AlertService,
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser
            .subscribe(user => {
                this.currentUser = user;
            });

        route.params.pipe(takeUntil(this.scopeUserSubject), pluck('id'))
            .subscribe(id => {
                if (id === 'all') {
                    this.settings = Object.assign({}, this.nonScopesettings );
                    this.allOfTrips = true;
                    this.userId = null;
                    this.source = this.fetchingAllData();
                } else {
                    this.settings = Object.assign({}, this.userScopesettings );
                    this.allOfTrips = false;
                    this.userId = id.toString();
                    this.source = this.fetchingUserData(this.userId);
                    this.getOneUserById(this.userId);
                }
            });
    }

    private getOneUserById(uid: string) {
        this.userService.userGetOne(uid)
            .pipe(first())
            .subscribe(
                (data: User) => {
                    this.scopeUser = data;
                    this.alertService.success("Get Scope User Successfully");
                    this.doc.text(`Trip Schedule for Next Month: 
                                  ${data.firstName} ${data.lastName}`, 14, 20);
                },
                error => {
                    this.alertService.error(error);
                });
    }

    private fetchingAllData(message = true): LocalDataSource {
        const fetchOption = this.timeSlot ? 'all' : 'future';
        return new TripSourceService(`/admin/trip/${fetchOption}`,
                                     this.http, this.alertService, true, message);
    }

    private fetchingUserData(userId, message = true): LocalDataSource {
        const fetchOption = this.timeSlot ? 'all' : 'future';
        return new TripSourceService(`/admin/trip/user/${fetchOption}/${userId}`,
                                     this.http, this.alertService, false, message);
    }

    public ngOnInit() {
        this.startTimeModel = null;
        this.doc = new JsPDF('l');
    }

    public switchOption() {
        if (this.allOfTrips) {
            this.source = this.fetchingAllData();
        } else {
            this.source = this.fetchingUserData(this.userId);
        }
    }

    onCreateConfirm(event): void {
        if (window.confirm('Are you sure you want to create?')) {
            const trip = event.newData;
            trip.userId = this.userId;
            this.tripService.adminCreate(trip).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Create Trip Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    onUpdateConfirm(event) {
        if (window.confirm('Are you sure you want to save?')) {
            const trip = event.newData;
            if (this.allOfTrips) {
                trip.userId = trip.user.userId;
            } else {
                trip.userId = this.userId;
            }
            this.tripService.adminUpdate(trip.id, trip).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Update Trip Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    onDeleteConfirm(event) {
        if (window.confirm('Are you sure you want to delete?')) {
            this.tripService.adminDelete(event.data.id).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Delete Trip Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    resetFilters() {
        this.startTimeModel = null;
        this.endTimeModel = null;
        this.source.reset();
    }

    preparePrintData(data: Trip[]) {
        const reports = [];
        data.forEach(e => {
            const currentStartOfDay = moment().local().startOf('day');
            const daysLeft = TripSourceService.calculateDaysLeft(
                moment(e.startTime).local().startOf('day').diff(currentStartOfDay, 'days')
            );

            e.startTime = moment(e.startTime).format('M/DD/YYYY');
            e.endTime = moment(e.endTime).format('M/DD/YYYY');

            if (this.allOfTrips) {
                reports.push(Trip.toListWithUser(daysLeft, new Trip(e)));
            } else {
                reports.push(Trip.toList(daysLeft, new Trip(e)));
            }

        });
        return reports;
    }

    preparePrintDocByUID(reports, offset) {
        this.doc.autoTable({
            head: [[' ', 'Country', 'State', 'City', 'Address', 'Zip Code',
                    'Start Date', 'End Date', 'Comment']],
            body: reports,
            startY: 25,
            styles: {overflow: 'ellipsize', cellWidth: 'wrap'},
            // Override the default above for the text column
            columnStyles: {text: {cellWidth: 'auto'}}
        });
        const nextMonth = moment().utcOffset(0 - offset).add(1, 'month').format('MMMM');
        this.doc.setProperties({
            title: `${this.scopeUser.firstName}_${this.scopeUser.lastName}_${nextMonth}`
        });
        window.open(this.doc.output('bloburl'));
    }

    preparePrintDoc(reports, offset) {
        this.doc.text('All Trip Schedule for Next Month', 14, 20);
        this.doc.autoTable({
            head: [[' ', 'Country', 'State', 'City', 'Address', 'Zip Code',
                    'Email', 'Start Date', 'End Date', 'Comment']],
            body: reports,
            startY: 25,
            styles: {overflow: 'ellipsize', cellWidth: 'wrap'},
            // Override the default above for the text column
            columnStyles: {text: {cellWidth: 'auto'}}
        });
        const nextMonth = moment().utcOffset(0 - offset).add(1, 'month').format('MMMM');
        this.doc.setProperties({
            title: `All_Trips_${nextMonth}`
        });
        window.open(this.doc.output('bloburl'));
    }

    fetchAllReport(offset) {
        this.tripService.adminPrint(offset)
            .pipe(first())
            .subscribe(
                (data: Trip[]) => {
                    const reports = this.preparePrintData(data);
                    this.preparePrintDoc(reports, offset);
                },
                error => {
                    this.alertService.error(error);
                });
    }

    fetchReportByUID(offset) {
        this.tripService.adminPrintByUID(this.userId, offset)
            .pipe(first())
            .subscribe(
                (data: Trip[]) => {
                    const reports = this.preparePrintData(data);
                    this.preparePrintDocByUID(reports, offset);
                },
                error => {
                    this.alertService.error(error);
                });
    }

    print() {
        const offset = new Date().getTimezoneOffset();
        if (this.allOfTrips) {
            this.fetchAllReport(offset);
        } else {
            this.fetchReportByUID(offset);
        }
    }

    backToAdminPage() {
        this.router.navigate(['/administration']);
    }

    public ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    private userScopesettings = {
        mode: 'inline',
        add: {
            confirmCreate: true,
        },
        edit: {
            confirmSave: true
        },
        delete: {
            confirmDelete: true
        },
        columns: {
            daysLeft: {
                title: '',
                type: 'string',
                width: '140px',
                filter: false,
                editable: false,
                addable: false,
            },
            country: {
                title: 'Country',
                type: 'number',
                width: '200px',
            },
            state: {
                title: 'State',
                type: 'string',
                width: '160px',
            },
            city: {
                title: 'City',
                type: 'string',
                width: '160px',
            },
            address: {
                title: 'Address',
                type: 'string',
                width: '220px',
            },
            zipCode: {
                title: 'Zip Code',
                type: 'string',
                width: '140px',
            },
            startTime: {
                title: 'Start Date',
                type: 'custom',
                renderComponent: DatepickerRenderComponent,
                filter: {
                    type: 'custom',
                    component: DatepickerFilterComponent,
                },
                editor: {
                    type: 'custom',
                    component: DatepickerEditorComponent,
                },
                width: '150px',
            },
            endTime: {
                title: 'End Date',
                type: 'custom',
                renderComponent: DatepickerRenderComponent,
                filter: {
                    type: 'custom',
                    component: DatepickerFilterComponent,
                },
                editor: {
                    type: 'custom',
                    component: DatepickerEditorComponent,
                },
                width: '150px',
            },
            comment: {
                title: 'Comment',
                filter: false,
                editor: {
                    type: 'textarea',
                },
                width: '280px',
            }
        },
        pager: {
            display: true,
            perPage: 8
        }
    };
    private nonScopesettings = {
        mode: 'inline',
        add: {
            confirmCreate: true,
        },
        edit: {
            confirmSave: true
        },
        delete: {
            confirmDelete: true
        },
        actions: {
            add: false
        },
        columns: {
            daysLeft: {
                title: '',
                type: 'string',
                width: '160px',
                filter: false,
                editable: false,
                addable: false,
            },
            country: {
                title: 'Country',
                type: 'number',
                width: '200px',
            },
            state: {
                title: 'State',
                type: 'string',
                width: '160px',
            },
            city: {
                title: 'City',
                type: 'string',
                width: '160px',
            },
            address: {
                title: 'Address',
                type: 'string',
                width: '220px',
            },
            zipCode: {
                title: 'Zip Code',
                type: 'string',
                width: '140px',
            },
            email: {
                title: 'User Email',
                type: 'string',
                width: '200px',
                editable: false,
                addable: false,
            },
            startTime: {
                title: 'Start Date',
                type: 'custom',
                renderComponent: DatepickerRenderComponent,
                filter: {
                    type: 'custom',
                    component: DatepickerFilterComponent,
                },
                editor: {
                    type: 'custom',
                    component: DatepickerEditorComponent,
                },
                width: '150px',
            },
            endTime: {
                title: 'End Date',
                type: 'custom',
                renderComponent: DatepickerRenderComponent,
                filter: {
                    type: 'custom',
                    component: DatepickerFilterComponent,
                },
                editor: {
                    type: 'custom',
                    component: DatepickerEditorComponent,
                },
                width: '150px',
            },
            comment: {
                title: 'Comment',
                filter: false,
                editor: {
                    type: 'textarea',
                },
                width: '280px',
            }
        },
        pager: {
            display: true,
            perPage: 8
        }
    };
}
