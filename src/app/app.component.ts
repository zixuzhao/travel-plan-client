import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from "@app/_models";
import { Role } from "@app/_models/role";

@Component({selector: 'app-component', templateUrl: 'app.component.html'})
export class AppComponent {
    public currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    public managerPermission(role: string) {
        return role === Role.ADMIN || role === Role.MANAGER
    }

    public profile() {
        this.router.navigate(['/profile']);
    }

    public manage() {
        if(this.managerPermission(this.currentUser.role)) {
            this.router.navigate(['/administration']);
        } else {
            this.router.navigate(['/']);
        }
    }

    public logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}
