import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { User } from '@app/_models';
import { AlertService, AuthenticationService, UserService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { first } from "rxjs/operators";
import { Self } from "@app/_models/Request/self";
import { CheckPwdResponse } from "@app/_models/Response/CheckPwdResponse";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";

@Component({templateUrl: 'profile.component.html'})
export class ProfileComponent implements OnInit, OnDestroy {
    public currentUser: User;
    public currentUserSubscription: Subscription;
    public updateProfileForm: FormGroup;
    public loading = false;
    public submitted = false;
    public closeResult: string;
    public truncateCheck: boolean = false;

    private static getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private modalService: NgbModal
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser
            .subscribe(user => {
                this.currentUser = user;
            });
    }

    public confirmDelete(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            if(result) {
                this.onDelete();
            }
        }, (reason) => {
            this.truncateCheck = false;
            this.closeResult = `Dismissed ${ProfileComponent.getDismissReason(reason)}`;
        });
    }

    private removeUnusedFields(target: object): void {
        Object.keys(target).forEach(key =>
            (target[key] === undefined || target[key] === '') && delete target[key]);
    }

    private onUpdate(self: Self) {
        this.userService.selfUpdate(self)
            .pipe(first())
            .subscribe(
                data => {
                    const user = new User(data, this.currentUser.token);
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.authenticationService.updateUser(user);
                    this.alertService.success('Update Successfully', false);
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.updateProfileForm.controls;
    }

    public ngOnInit() {
        this.updateProfileForm = this.formBuilder.group({
            firstName: [this.currentUser.firstName],
            lastName: [this.currentUser.lastName],
            email: [this.currentUser.email, Validators.email],
            username: [this.currentUser.username],
            currentPassword: ['', [Validators.minLength(6), Validators.required]],
            password: ['', Validators.minLength(6)],
        });
    }

    public onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.updateProfileForm.invalid) {
            return;
        }
        this.loading = true;
        const self = new Self(this.updateProfileForm.value);
        this.removeUnusedFields(self);

        this.userService.getAuth(this.currentUser.id,
                                 this.updateProfileForm.value.currentPassword)
            .pipe(first())
            .subscribe(
                (data: CheckPwdResponse) => {
                    if (data && data.success && data.data) {
                        this.onUpdate(self);
                    } else {
                        this.alertService.error(data.message);
                        this.loading = false;
                    }
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            );
    }

    onDelete() {
        this.loading = true;

        if (this.truncateCheck) {
            this.userService.selfTruncate()
                .pipe(first())
                .subscribe(
                    data => {
                        this.alertService.success(
                            `Delete user ${this.currentUser.username} successfully`,
                            true
                        );
                        this.authenticationService.logout();
                        this.router.navigate(['/login']);
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    }
                );
        } else {
            this.userService.selfDelete()
                .pipe(first())
                .subscribe(
                    data => {
                        this.alertService.success(
                            `Delete user ${this.currentUser.username} successfully`,
                            true
                        );
                        this.authenticationService.logout();
                        this.router.navigate(['/login']);
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    }
                );
        }
    }

    public ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }
}
