export class tripRequest {
    public country: string;
    public state: string;
    public city: string;
    public address: string;
    public zipCode: string;
    public startTime: string;
    public endTime: string;
    public comment: string;
    public userId: string;

    constructor(trip) {
        this.country = trip.country;
        this.state = trip.state;
        this.city = trip.city;
        this.address = trip.address;
        this.zipCode = trip.zipCode;
        this.startTime = trip.startTime;
        this.endTime = trip.endTime;
        this.comment = trip.comment;
        this.userId = trip.userId;
    }
}
