import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LocalDataSource } from 'ng2-smart-table';
import { User } from '@app/_models';
import { AlertService, AuthenticationService } from '@app/_services';
import { DatepickerEditorComponent, DatepickerFilterComponent, DatepickerRenderComponent } from "@app/datepicker";
import { TripService } from "@app/_services/trip.service";
import { TripSourceService } from "@app/_services/source.trip.service";
import { HttpClient } from "@angular/common/http";
import { first } from "rxjs/operators";
import JsPDF from 'jspdf';
import 'jspdf-autotable';
import { Trip } from "@app/_models/trip";
import * as moment from "moment";

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
    public currentUser: User;
    public currentUserSubscription: Subscription;

    public startTimeModel: Date;
    public endTimeModel: Date;

    public timeSlot: boolean = true;
    public source: LocalDataSource;

    public doc: JsPDF;
    constructor(
        private authenticationService: AuthenticationService,
        private tripService: TripService,
        private alertService: AlertService,
        private http: HttpClient,
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser
            .subscribe(user => {
                this.currentUser = user;
            });
    }

    private fetchingData(message = true): LocalDataSource {
        const fetchOption = this.timeSlot ? 'all' : 'future';
        return new TripSourceService(`/trips/self/fetch/${fetchOption}`,
                                     this.http, this.alertService, false, message);
    }

    public ngOnInit() {
        this.startTimeModel = null;
        this.source = this.fetchingData();
        this.doc = new JsPDF('l');
        this.doc.text(`Trip Schedule for Next Month: 
                      ${this.currentUser.firstName} ${this.currentUser.lastName}`, 14, 20);

    }

    public switchOption() {
        this.source = this.fetchingData();
    }

    onCreateConfirm(event): void {
        if (window.confirm('Are you sure you want to create?')) {
            const trip = event.newData;
            trip.userId = this.currentUser.id;
            this.tripService.selfCreate(trip).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Create Trip Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    onUpdateConfirm(event) {
        if (window.confirm('Are you sure you want to save?')) {
            const trip = event.newData;
            this.tripService.selfUpdate(trip.id, trip).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Update Trip Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    onDeleteConfirm(event) {
        if (window.confirm('Are you sure you want to delete?')) {
            this.tripService.selfDelete(event.data.id).pipe(first())
                .subscribe(
                    data => {
                        this.source.refresh();
                        this.alertService.success("Delete Trip Successfully");
                    },
                    error => {
                        this.source.refresh();
                        this.alertService.error(error);
                    });
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }

    preparePrintDataByUID(data: Trip[]) {
        const reports = [];
        data.forEach(e => {
            const currentStartOfDay = moment().local().startOf('day');
            const daysLeft = TripSourceService.calculateDaysLeft(
                moment(e.startTime).local().startOf('day').diff(currentStartOfDay, 'days')
            );

            e.startTime = moment(e.startTime).format('M/DD/YYYY');
            e.endTime = moment(e.endTime).format('M/DD/YYYY');

            reports.push(Trip.toList(daysLeft, new Trip(e)));
        });
        return reports;
    }

    preparePrintDocByUID(reports, offset) {
        this.doc.autoTable({
            head: [[' ', 'Country', 'State', 'City', 'Address', 'Zip Code', 'Start Date', 'End Date', 'Comment']],
            body: reports,
            startY: 25,
            styles: {overflow: 'ellipsize', cellWidth: 'wrap'},
            columnStyles: {text: {cellWidth: 'auto'}}
        });
        const nextMonth = moment().utcOffset(0 - offset).add(1, 'month').format('MMMM');
        this.doc.setProperties({
            title: `${this.currentUser.firstName}_${this.currentUser.lastName}_${nextMonth}`
        });
        window.open(this.doc.output('bloburl'));
    }

    print() {
        const offset = new Date().getTimezoneOffset();
        this.tripService.selfPrint(offset)
            .pipe(first())
            .subscribe(
                (data: Trip[]) => {
                    const reports = this.preparePrintDataByUID(data);
                    this.preparePrintDocByUID(reports, offset);
                },
                error => {
                    this.alertService.error(error);
                });
    }

    resetFilters() {
        this.startTimeModel = null;
        this.endTimeModel = null;
        this.source.reset();
    }

    public ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    public settings = {
        mode: 'inline',
        add: {
            confirmCreate: true,
        },
        edit: {
            confirmSave: true
        },
        delete: {
            confirmDelete: true
        },
        columns: {
            daysLeft: {
                title: '',
                type: 'string',
                width: '140px',
                filter: false,
                editable: false,
                addable: false,
            },
            country: {
                title: 'Country',
                type: 'number',
                width: '200px',
            },
            state: {
                title: 'State',
                type: 'string',
                width: '160px',
            },
            city: {
                title: 'City',
                type: 'string',
                width: '160px',
            },
            address: {
                title: 'Address',
                type: 'string',
                width: '220px',
            },
            zipCode: {
                title: 'Zip Code',
                type: 'string',
                width: '140px',
            },
            startTime: {
                title: 'Start Date',
                type: 'custom',
                renderComponent: DatepickerRenderComponent,
                filter: {
                    type: 'custom',
                    component: DatepickerFilterComponent,
                },
                editor: {
                    type: 'custom',
                    component: DatepickerEditorComponent,
                },
                width: '150px',
            },
            endTime: {
                title: 'End Date',
                type: 'custom',
                renderComponent: DatepickerRenderComponent,
                filter: {
                    type: 'custom',
                    component: DatepickerFilterComponent,
                },
                editor: {
                    type: 'custom',
                    component: DatepickerEditorComponent,
                },
                width: '150px',
            },
            comment: {
                title: 'Comment',
                filter: false,
                editor: {
                    type: 'textarea',
                },
                width: '280px',
            }
        },
        pager: {
            display: true,
            perPage: 8
        }
    };
}
