import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { tripRequest } from "@app/_models/Request/tripRequest";

@Injectable({providedIn: 'root'})
export class TripService {
    constructor(private http: HttpClient) {
    }

    public selfCreate(trip: object) {
        return this.http.post(`${environment.apiUrl}/trips/self/create`, new tripRequest(trip));
    }

    public selfUpdate(id: string, trip: object) {
        return this.http.put(`${environment.apiUrl}/trips/self/update/${id}`, new tripRequest(trip));
    }

    public selfDelete(id: string) {
        return this.http.delete(`${environment.apiUrl}/trips/self/delete/${id}`);
    }

    public selfPrint(offset: number) {
        return this.http.get(`${environment.apiUrl}/trips/self/report/${offset}`);
    }

    public adminCreate(trip: object) {
        return this.http.post(`${environment.apiUrl}/admin/trip/create`, new tripRequest(trip));
    }

    public adminUpdate(id: string, trip: object) {
        return this.http.put(`${environment.apiUrl}/admin/trip/update/${id}`, new tripRequest(trip));
    }

    public adminDelete(id: string) {
        return this.http.delete(`${environment.apiUrl}/admin/trip/delete/${id}`);
    }

    public adminPrint(offset: number) {
        return this.http.get(`${environment.apiUrl}/admin/trip/report/all/${offset}`);
    }

    public adminPrintByUID(id: string, offset: number) {
        return this.http.get(`${environment.apiUrl}/admin/trip/report/user/${id}/${offset}`);
    }
}
