import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { DefaultEditor, DefaultFilter, ViewCell } from 'ng2-smart-table';
import * as moment from 'moment';

@Component({
    selector: 'smart-table-datepicker',
    templateUrl: 'datepicker.component.html',
    styleUrls: ['datepicker.component.css']
})
export class DatepickerEditorComponent extends DefaultEditor implements OnInit {

    @Input() placeholder: string = 'Select Date';

    inputModel: Date;

    constructor() {
        super();
    }

    ngOnInit() {
        if (this.cell && this.cell.newValue) {
            this.inputModel = new Date(this.cell.newValue);
            this.cell.newValue = moment(this.inputModel).format('M/DD/YYYY');

        }

        if (this.cell && !this.inputModel) {
            this.inputModel = new Date(this.cell.getValue());
            this.cell.newValue = moment(this.inputModel).format('M/DD/YYYY');
        }
    }

    onChange() {
        if (this.cell && this.inputModel) {
            this.cell.newValue = moment(this.inputModel).format('M/DD/YYYY');
        }
    }
}

@Component({
    selector: 'smart-table-datepicker-filter',
    templateUrl: 'datepicker.filter.component.html',
    styleUrls: ['datepicker.component.css']
})
export class DatepickerFilterComponent extends DefaultFilter implements OnInit, OnChanges {

    @Input() placeholder: string = 'Select Date';

    inputModel: Date;

    constructor() {
        super();
    }

    ngOnInit() {
    }

    onChange() {
        this.query = this.inputModel !== undefined ? moment(this.inputModel).format('M/DD/YYYY') : '';
        this.setFilter();
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.query && changes.query.currentValue === '') {
            this.inputModel = undefined;
        }
    }
}

@Component({
    template: `{{value}}`,
})
export class DatepickerRenderComponent implements ViewCell, OnInit {
    @Input() value: string;
    @Input() rowData: any;

    constructor() {
    }

    ngOnInit() {
    }

}
